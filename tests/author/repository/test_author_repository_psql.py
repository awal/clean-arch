import pytest

from src.app import connect_db
from src.author.repository.author_repository_psql import AuthorRepositoryPSQL
from src.author.use_cases.author_use_cases import AuthorUseCases


@pytest.fixture(scope="module")
def db_con():
    db = connect_db()
    yield db
    db.disconnect()


def test_author_repository_get_by_id(db_con):
    author_repo = AuthorRepositoryPSQL(db=db_con)
    author_use_case = AuthorUseCases(repo=author_repo)
    author = author_use_case.get_by_id(pk=1)

    assert author.name == 'boncel'


def test_author_repository_get_unknown_id(db_con):
    author_repo = AuthorRepositoryPSQL(db=db_con)
    author_use_case = AuthorUseCases(repo=author_repo)
    author = author_use_case.get_by_id(pk=-1)

    assert author is None
