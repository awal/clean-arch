from src.author.domain.author import Author


def test_author_model_init():
    id = 1
    author = Author(
        id=id,
        name='boncel',
        created_at='2016-01-11',
        updated_at='2016-01-12'
    )

    assert author.id == id
    assert author.name == 'boncel'
    assert author.created_at == '2016-01-11'
    assert author.updated_at == '2016-01-12'


def test_author_model_from_dict():
    id = 1
    author = Author.from_dict({
        'id': id,
        'name': 'boncel',
        'created_at': '2016-01-11',
        'updated_at': '2016-01-12'
    })

    assert author.id == id
    assert author.name == 'boncel'
    assert author.created_at == '2016-01-11'
    assert author.updated_at == '2016-01-12'
