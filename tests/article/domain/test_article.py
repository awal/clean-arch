import pytest

from src.article.domain.article import Article
from src.author.domain.author import Author


@pytest.fixture(scope='module')
def author():
    author_data = Author(
        id=1,
        name='boncel',
        created_at='2016-01-11',
        updated_at='2016-01-12'
    )
    return author_data


def test_article_model_init(author):
    id = 1
    article = Article(
        id=id,
        title='boncel babang',
        content='boncel in fairy land',
        author=author,
        created_at='2018-01-11',
        updated_at='2018-01-12'
    )

    assert article.id == id
    assert article.title == 'boncel babang'
    assert article.content == 'boncel in fairy land'
    assert article.author.name == author.name
    assert article.created_at == '2018-01-11'
    assert article.updated_at == '2018-01-12'


def test_article_model_from_dict(author):
    id = 1
    article = Article.from_dict({
        'id': id,
        'title': 'boncel babang',
        'content': 'boncel in fairy land',
        'author': {
            'id': author.id,
            'name': author.name,
            'created_at': author.created_at,
            'updated_at': author.updated_at
        },
        'created_at': '2018-01-11',
        'updated_at': '2018-01-12'
    })

    assert article.id == id
    assert article.title == 'boncel babang'
    assert article.content == 'boncel in fairy land'
    assert article.author.name == author.name
    assert article.created_at == '2018-01-11'
    assert article.updated_at == '2018-01-12'
