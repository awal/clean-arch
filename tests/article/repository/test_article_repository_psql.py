import pytest

from src.shared import helper
from src.app import connect_db
from src.article.domain.article import Article
from src.article.repository.article_repository_psql import ArticleRepositoryPSQL
from src.author.repository.author_repository_psql import AuthorRepositoryPSQL
from src.author.use_cases.author_use_cases import AuthorUseCases


pytest.global_id = 0


@pytest.fixture(scope="module")
def db_con():
    db = connect_db()
    yield db
    db.disconnect()


def test_article_repository_create(db_con):
    author_repo = AuthorRepositoryPSQL(db=db_con)
    author_use_cases = AuthorUseCases(repo=author_repo)
    author = author_use_cases.get_by_id(pk=1)

    article_data = Article(
        id=None,
        title='Boncel petualang',
        content='Boncel berpetualang kesana kesini sama betty',
        author=author,
        created_at=helper.get_now_timestamp(),
        updated_at=helper.get_now_timestamp()
    )

    article_repo = ArticleRepositoryPSQL(db=db_con)
    id = article_repo.create(article=article_data)
    pytest.global_id = id

    assert isinstance(id, int)
    assert id > 0


def test_article_repository_get_by_id(db_con):
    id = pytest.global_id
    article_repo = ArticleRepositoryPSQL(db=db_con)
    article = article_repo.get_by_id(pk=id)

    assert article.id == id
    assert article.author.id == 1


def test_article_repository_update(db_con):
    id = pytest.global_id
    article_repo = ArticleRepositoryPSQL(db=db_con)
    article = article_repo.get_by_id(pk=id)
    article.title = 'Di update jadi judul lain'

    article_updated = article_repo.update(article)

    assert isinstance(article_updated, int)
    assert article_updated == 1


def test_article_repository_get_all(db_con):
    article_repo = ArticleRepositoryPSQL(db=db_con)
    filters = {
        'sort_field': 'created_at'
    }
    articles = article_repo.get_all(filters)
    

    assert len(articles) > 0


@pytest.mark.skip(reason="not run this test because data needed for testing list")
def test_article_repository_delete(db_con):
    id = pytest.global_id
    article_repo = ArticleRepositoryPSQL(db=db_con)
    article_deleted = article_repo.delete(id)
    assert isinstance(article_deleted, int)
    assert article_deleted == 1

