from orator import DatabaseManager
from sanic import Sanic

from config import Config
from src.article.delivery.index import bp_article
from src.author.delivery.http.index import bp_author


def create_app(config_object=Config):

    app = Sanic(__name__)
    app.config.from_object(config_object)
    app.blueprint(bp_author)
    app.blueprint(bp_article)

    return app


def connect_db():
    config = {
        'postgresql': {
            'driver': 'pgsql',
            'host': Config.DB_HOST,
            'database': Config.DB_NAME,
            'user': Config.DB_USER,
            'password': Config.DB_PASS,
            'prefix': ''
        }
    }

    return DatabaseManager(config)
