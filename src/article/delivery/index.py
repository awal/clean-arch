from sanic import Blueprint
from sanic.response import json

from src.article.repository.article_repository_psql import ArticleRepositoryPSQL
from src.article.use_cases.article_use_cases import ArticleUseCases
from src.shared import helper

bp_article = Blueprint('article', url_prefix='article')


@bp_article.route('/')
async def index(request):
    adict = request.raw_args
    article_repo = ArticleRepositoryPSQL(db=request.app.db)
    article_use_case = ArticleUseCases(repo=article_repo)
    article = article_use_case.get_all(adict)

    cur_page = article['current_page']

    return json({
        'total': article['total'],
        'limit': article['per_page'],
        'page': cur_page,
        'next': helper.build_next_url(request.url, cur_page, article['next_page']),
        'prev': helper.build_prev_url(request.url, cur_page, article['prev_page']),
        'has_next': article['has_more_pages'],
        'result': article['result']
    })