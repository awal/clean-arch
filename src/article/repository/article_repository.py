from abc import ABCMeta, abstractmethod


class ArticleRepository(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_all(self): pass

    @abstractmethod
    def get_by_id(self, pk): pass

    @abstractmethod
    def create(self, article): pass

    @abstractmethod
    def update(self): pass

    @abstractmethod
    def delete(self): pass

    @abstractmethod
    def get_total(self, adict=None): pass
