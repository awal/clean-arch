from config import Config

from src.shared import helper
from src.article.domain.article import Article
from src.article.repository.article_repository import ArticleRepository
from src.author.repository.author_repository_psql import AuthorRepositoryPSQL


class ArticleRepositoryPSQL(ArticleRepository):
    def __init__(self, db):
        self.db = db
        super(ArticleRepositoryPSQL, self).__init__()

    def get_all(self, adict):

        query = self._filter_query(adict)
        result = []
        author_repo = AuthorRepositoryPSQL(db=self.db)
        for row in query:
            author = author_repo.get_by_id(pk=row['author_id'])
            article = Article.from_dict({
                'id': row['id'],
                'title': row['title'],
                'content': row['content'],
                'author': {
                    'id': author.id,
                    'name': author.name,
                    'created_at': author.created_at,
                    'updated_at': author.updated_at
                },
                'created_at': row['created_at'],
                'updated_at': row['updated_at']
            })
            result.append(article)

        return {
            'count': query.count(),
            'current_page': query.current_page,
            'has_more_pages': query.has_more_pages(),
            'last_page': query.last_page,
            'next_page': query.next_page,
            'per_page': query.per_page,
            'prev_page': query.previous_page,
            'total': query.total,
            'result': result
        }

    def get_by_id(self, pk):
        query = self.db.table('article').where('id', pk).first()
        if query:
            return Article.from_dict({
                'id': query['id'],
                'title': query['title'],
                'content': query['content'],
                'author': {
                    'id': query['author_id'],
                },
                'created_at': query['created_at'],
                'updated_at': query['updated_at']
            })
        return query

    def create(self, article):
        return self.db.table('article').insert_get_id({
            'title': article.title,
            'content': article.content,
            'author_id': article.author.id,
            'created_at': helper.get_now_timestamp(),
            'updated_at': helper.get_now_timestamp()
        })

    def update(self, article):
        return self.db.table('article').where('id', article.id).update({
            'title': article.title,
            'content': article.content,
            'author_id': article.author.id,
            'updated_at': helper.get_now_timestamp()
        })

    def delete(self, pk):
        return self.db.table('article').where('id', pk).delete()

    def get_total(self, adict=None):
        return self.db.table('table').count()

    def _filter_query(self, adict):
        query = self.db.table('article')

        page = helper.get_value_from_dict(adict, 'page', 1)
        limit = helper.get_value_from_dict(adict, 'limit', Config.LIMIT)

        title = helper.get_value_from_dict(adict, 'title', '')
        if title.strip() != "":
            query = query.where('title', 'like', "%{}%".format(title))

        sort = helper.get_value_from_dict(adict, 'sort', 'created_at')
        if sort == '-created_at':
            query = query.order_by('created_at', 'DESC')
        else:
            query = query.order_by('created_at', 'ASC')

        return query.paginate(limit, page)
