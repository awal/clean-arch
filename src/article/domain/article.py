from src.author.domain.author import Author


class Article(object):
    def __init__(self, id, title, content, author, created_at, updated_at):
        self.id = id
        self.title = title
        self.content = content
        self.author = author
        self.created_at = created_at
        self.updated_at = updated_at

    @classmethod
    def from_dict(self, adict):
        article = Article(
            id=adict['id'],
            title=adict['title'],
            content=adict['content'],
            author=Author.from_dict(adict['author']),
            created_at=adict['created_at'],
            updated_at=adict['updated_at']
        )

        return article
