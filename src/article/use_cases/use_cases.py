from abc import ABCMeta, abstractmethod


class ArticleBaseUseCases(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_all(self, adict=None): pass

    @abstractmethod
    def get_by_id(self, pk): pass

    @abstractmethod
    def create(self, adict): pass

    @abstractmethod
    def update(self): pass

    @abstractmethod
    def delete(self): pass

    @abstractmethod
    def get_total(self, adict=None): pass