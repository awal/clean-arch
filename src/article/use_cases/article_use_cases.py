from src.article.use_cases.use_cases import ArticleBaseUseCases


class ArticleUseCases(ArticleBaseUseCases):

    def __init__(self, repo):
        self.repo = repo
        super(ArticleBaseUseCases, self).__init__()

    def get_all(self, adict=None):
        return self.repo.get_all(adict)

    def get_by_id(self, pk): pass

    def create(self, adict): pass

    def update(self): pass

    def delete(self): pass

