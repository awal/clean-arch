from src.author.domain.author import Author
from src.author.repository.author_repository import AuthorRepository


class AuthorRepositoryPSQL(AuthorRepository):
    def __init__(self, db):
        self.db = db
        super(AuthorRepositoryPSQL, self).__init__()

    def get_by_id(self, pk):
        query = self.db.table('author').where('id', pk).first()
        if query:
            return Author.from_dict(query)
        return query

