from sanic import Blueprint
from sanic.response import json

from src.author.repository.author_repository_psql import AuthorRepositoryPSQL
from src.author.use_cases.author_use_cases import AuthorUseCases

bp_author = Blueprint('author', url_prefix='author')


@bp_author.route('/')
async def index(request):
    author_repo = AuthorRepositoryPSQL(db=request.app.db)
    author_use_case = AuthorUseCases(repo=author_repo)
    author = author_use_case.get_by_id(pk=1)
    return json({'author': author})


@bp_author.route('/test')
async def index(request):
    print(request.raw_args)
    return json({'author': "tos"})

