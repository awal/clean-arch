from src.author.use_cases.use_cases import AuthorBaseUseCases


class AuthorUseCases(AuthorBaseUseCases):
    def __init__(self, repo):
        self.repo = repo
        super(AuthorUseCases, self).__init__()

    def get_by_id(self, pk):
        return self.repo.get_by_id(pk)
