from abc import ABCMeta, abstractmethod


class AuthorBaseUseCases(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_by_id(self, pk):
        pass