class Author(object):
    def __init__(self, id, name, created_at, updated_at):
        self.id = id
        self.name = name
        self.created_at = created_at
        self.updated_at = updated_at

    @classmethod
    def from_dict(cls, adict):
        author = Author(
            id=adict['id'],
            name=adict['name'] if 'name' in adict else "",
            created_at=adict['created_at'] if 'created_at' in adict else "",
            updated_at=adict['updated_at'] if 'updated_at' in adict else ""
        )

        return author
