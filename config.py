import os

from decouple import config


class Config(object):
    """Base configuration."""

    APP_DIR = os.path.abspath(os.path.dirname(__file__))  # This directory
    PROJECT_ROOT = os.path.abspath(os.path.join(APP_DIR, os.pardir))

    ENV = config('ENV')
    TESTING = config('TESTING', cast=bool)
    DEBUG = config('DEBUG', cast=bool)

    DB_HOST = config('DB_HOST')
    DB_NAME = config('DB_NAME')
    DB_USER = config('DB_USER')
    DB_PASS = config('DB_PASS')

    LIMIT = config('LIMIT', cast=int)
